require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get login" do
    get users_login_url
    assert_response :success
  end

  test "should get forget_password" do
    get users_forget_password_url
    assert_response :success
  end

  test "should get new_password" do
    get users_new_password_url
    assert_response :success
  end

  test "should get update_login" do
    get users_update_login_url
    assert_response :success
  end

end
