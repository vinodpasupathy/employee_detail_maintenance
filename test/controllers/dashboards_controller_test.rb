require 'test_helper'

class DashboardsControllerTest < ActionDispatch::IntegrationTest
  test "should get active_announcements" do
    get dashboards_active_announcements_url
    assert_response :success
  end

  test "should get current_absentees" do
    get dashboards_current_absentees_url
    assert_response :success
  end

  test "should get birthday_remainder" do
    get dashboards_birthday_remainder_url
    assert_response :success
  end

  test "should get employee_remainder" do
    get dashboards_employee_remainder_url
    assert_response :success
  end

end
