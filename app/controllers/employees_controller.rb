class EmployeesController < ApplicationController
  def index
    @employees = Employee.all
    render json: @employees 
  end

  def create
    @employee = Employee.new(employee_param)
    if @employee.save
      render json: @employee
    else
      render json: {status: "error", code: 400, message: "employee not created"}
    end
  end

  def update
    @employee = Employee.find(params[:id])
    if @employee.update(employee_param)
      render json: @employee
    else
      render json: {status: "error", code: 400, message: "employee updation failed"}
    end
  end

  def destory
    @employee = Employee.find(params[:id])
    if @employee.delete
      render json: @employee
    else
      render json: {status: "error", code: 400, message: "employee deletion failed"}
    end
  end
private
 def employee_param
  Employee.require(:employee).permit(:email,:date_of_joining,:date_of_end,:date_of_birth,:blood_group,:mobile,:gender,:name,:address)
 end
end
