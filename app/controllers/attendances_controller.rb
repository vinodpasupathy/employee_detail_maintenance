class AttendancesController < ApplicationController
  def index
    @attendences = Attendence.all
    render json: @attendences
  end

  def create
    @attendence = Attendence.new(attendence_param)
    if @attendence.save
      render json: @attendence
    else
      render json: {status: "error", code: 400, message: "Attendence not created"}
    end
  end

  def update
    @attendence = Attendence.find(params[:id])
    if @attendence.update(attendence_param)
      render json: @attendence
    else
      render json: {status: "error", code: 400, message: "Attendence not updated"}
    end
  end

  def destory
    @attendence = Attendence.find(params[:id])
    if @attendence.delete
      render json: @attendence
    else
      render json: {status: "error", code: 400, message: "Attendence delition failed"}
    end
  end

private

 def attendence_param
  Attendence.require(:attendence).permit(:employee_id,:reason,:current_date)
 end

end
