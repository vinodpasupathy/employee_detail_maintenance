require 'bcrypt'
class UsersController < ApplicationController
include BCrypt
 
  def check_username
    @email = User.find_by(email: params[:email])
      if @email
        render json: true
      else
	render json: {status: "error", code: 404, message: "Email is incorrect"}
       end
  end

  def login
    @login = User.find(email: login_params[:email])
      if @login && BCrypt::Password.new(@login.password) == login_params[:password]
        render json: true
      else
        render json: {status: "error", code: 401, message: "username or password is incorrect"}
      end
  end

  def forget_password
    
  end

  def new_password
        @new_password = User.find(current_user).update(password: params[:password])
      if @new_password
        render json: true
      else
        render json: {status: "error", code: 404, message: "password is not changed"}
      end
  end

  def update_login
        @update_status = User.find(current_user).update(first_login: false)
      if @update_status
        render json: true
      else
        render json: {status: "error", code: 404, message: "password is not changed"}
      end
  end
#@user = User.new(email: "admin@gmail.com", password: BCrypt::Password.create("admin@123"), role: "admin", first_login: true)
  private
  def login_params
        User.require(:login).permit(:email,:password)
  end
end
