class AnnouncementsController < ApplicationController
  def index
    @announcements = Announcement.all
    render json: @announcements
  end

  def create
    @announcement = Announcement.new(announcement_param)
    if @announcement.save
      render json: @announcement
    else
      render json: {status: "error", code: 400, message: "@announcement not created"}
    end
  end

  private

 def announcement_param
  Attendence.require(:attendence).permit(:employee_id,:reason,:current_date)
 end
end
