class DashboardsController < ApplicationController
  def active_announcements
    @attendences = Announcement.where(end_date <= Date.current)
    render json: @attendences
  end

  def current_absentees
    @attendences = Attendence.where(current_date == Date.current)
    render json: @attendences
  end

  def birthday_remainder
    @attendences = Employee.where(date_of_birth == Date.current)
    render json: @attendences
  end

  def employee_remainder
  end
end
