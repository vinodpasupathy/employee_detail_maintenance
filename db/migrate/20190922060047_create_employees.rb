class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :email
      t.date :date_of_joining
      t.date :date_of_end
      t.date :date_of_birth
      t.string :blood_group
      t.integer :mobile
      t.string :gender
      t.string :name
      t.text :address

      t.timestamps
    end
  end
end
