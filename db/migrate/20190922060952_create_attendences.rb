class CreateAttendences < ActiveRecord::Migration[5.2]
  def change
    create_table :attendences do |t|
      t.references :employee, foreign_key: true
      t.text :reason
      t.date :current_date

      t.timestamps
    end
  end
end
