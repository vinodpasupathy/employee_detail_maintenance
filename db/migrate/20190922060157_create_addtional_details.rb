class CreateAddtionalDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :addtional_details do |t|
      t.json :extra_info
      t.text :document_submitted
      t.string :document_attached

      t.timestamps
    end
  end
end
